const path = require("path");
const webpack = require("webpack");
const paths = require("./config/webpack").paths;
const outputFiles = require("./config/webpack").outputFiles;
const rules = require("./config/webpack").rules;
const plugins = require("./config/webpack").plugins;
const resolve = require("./config/webpack").resolve;
const IS_PRODUCTION = require("./config/webpack").IS_PRODUCTION;
const IS_DEVELOPMENT = require("./config/webpack").IS_DEVELOPMENT;
const devServer = require("./config/webpack").devServer;
const pathHelper = require("./config/path-helper");

const entry = [
  "babel-polyfill",
  path.join(paths.javascript, "index.js")
];

plugins.push(new webpack.DefinePlugin({
  "process.env": {
    NODE_ENV: JSON.stringify("production"),
  }
}));

module.exports = {
  devtool: false,
  context: paths.javascript,
  entry,
  output: {
    path: path.join(__dirname, "public"),
    publicPath: "/public/",
    filename: outputFiles.client,
  },
  plugins,
  module: {
    rules
  },
  resolve,
  devServer
};
