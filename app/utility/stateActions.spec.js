import StateActions from "./stateActions";

describe("Utility stateActions: updateObject()", () => {
  it("should create a new object", () => {
    expect(StateActions.updateObject({}, {})).toEqual({});
  });

  it("should not modify the original object", () => {
    const objectToUpdate = {
      propertyA: 1,
      propertyB: ["1", "2", "3"]
    };
    const originalObjectState = JSON.stringify(objectToUpdate);

    const expectedObject = {
      propertyA: 2,
      propertyB: ["1", "2"]
    };

    const actualObject = StateActions.updateObject(objectToUpdate, expectedObject);
    expect(actualObject).toEqual(expectedObject);
    expect(objectToUpdate).toEqual(JSON.parse(originalObjectState));
  });
});

describe("Utility stateActions: updateArrayById()", () => {
  let originalArray;
  let expectedArray;
  let itemsToChange;
  let callBackFunction;

  beforeEach(() => {
    originalArray = [{
      id: 1,
      propertyA: "String 1",
      propertyB: 25
    },
    {
      id: 2,
      propertyA: "String 2",
      propertyB: 42
    },
    {
      id: 3,
      propertyA: "String 3",
      propertyB: 1
    }];

    expectedArray = [{
      id: 1,
      propertyA: "String 1",
      propertyB: 25
    },
    {
      id: 2,
      propertyA: "Changed String",
      propertyB: 0
    },
    {
      id: 3,
      propertyA: "Another changed string",
      propertyB: 0
    }];

    itemsToChange = [2, 3];

    callBackFunction = (item) => {
      if (item.id === 2) {
        return {
          id: 2,
          propertyA: "Changed String",
          propertyB: 0
        };
      }
      if (item.id === 3) {
        return {
          id: 3,
          propertyA: "Another changed string",
          propertyB: 0
        };
      }
      return item;
    };
  });

  it("should return an array", () => {
    expect(StateActions.updateArrayById([], [], item => item)).toEqual([]);
  });

  it("should return an updated array contained the transformed items", () => {
    expect(StateActions.updateArrayById(originalArray, itemsToChange, callBackFunction)).toEqual(expectedArray);
  });

  it("should not modify the original array", () => {
    const capturedOriginalArray = JSON.stringify(originalArray);
    StateActions.updateArrayById(originalArray, itemsToChange, callBackFunction);
    expect(originalArray).toEqual(JSON.parse(capturedOriginalArray));
  });
});
