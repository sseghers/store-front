export const replaceAll = (targetString, searchToken, replacementToken) => {
  return targetString.split(searchToken).join(replacementToken);
};
