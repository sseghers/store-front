export function convertNullData(value, toString = false) {
  if (value === '"null"') {
    return (toString)
      ? ""
      : undefined;
  }
  return value;
}

export function toUpperCaseFirst(word) {
  return word.charAt(0).toUpperCase() + word.slice(1, word.length);
}

export function toParsedPrice(price) {
  return parseFloat((price.substring(1)));
}
