function addToArray(originalArray, item) {
  return originalArray.concat(item);
}

function updateObject(oldObject, newValues) {
  return Object.assign({}, oldObject, newValues);
}

function updateArrayById(array, itemIds, itemCallback) {
  return array.map((item) => {
    if (!itemIds.find(i => i === item.id)) {
      return item;
    }

    return itemCallback(array.find(i => i.id === item.id));
  });
}


export default {
  addToArray,
  updateObject,
  updateArrayById
};
