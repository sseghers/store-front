import { selectAllCategoryItems } from "./../product/product";
import { toParsedPrice } from "./../utility/formators";

export const ADD_TO_CART = "store_front/cart/ADD_TO_CART";
export const REMOVE_FROM_CART = "store_front/cart/REMOVE_FROM_CART";
export const MODIFY_COUNT = "store_front/cart/MODIFY_COUNT";
export const TOGGLE_CART = "store_front/cart/SHOW_CART";

let initialState;
(function loadInitialState() {
  initialState = {
    inCart: {},
    showCartPreview: false
  };
})();

export function selectInCart(state) {
  return state.inCart || state.cart.inCart;
}

export function selectCartPreviewVisibility(state) {
  return (state.showCartPreview !== undefined)
    ? state.showCartPreview
    : state.cart.showCartPreview;
}

export function selectProductIsInCart(state, product) {
  return !!(selectInCart(state)[product.sku]);
}

export function selectProductCounter(state, product) {
  return selectInCart(state)[product.sku].counter;
}

export function selectCartItems(state) {
  const productsInCart = selectInCart(state);
  const cartKeys = Object.keys(productsInCart);

  if (cartKeys.length === 0) {
    return [];
  }

  return cartKeys
  .map(key => productsInCart[key])
  .sort((a, b) => b.addedTimestamp - a.addedTimestamp);
}

export function selectCartProductCount(state) {
  const productsInCart = selectInCart(state);
  const cartKeys = Object.keys(productsInCart);
  if (cartKeys.length < 1) {
    return 0;
  }
  return cartKeys.reduce((acc, productKey) => {
    return acc + parseInt(productsInCart[productKey].counter);
  }, 0);
}

export function selectProductTotal(state) {
  const productsInCart = selectInCart(state);
  const cartKeys = Object.keys(productsInCart);
  if (cartKeys.length < 1) {
    return 0;
  }
  return cartKeys.reduce((acc, productKey) => {
    const currentItem = productsInCart[productKey];
    return Math.round((acc + (toParsedPrice(currentItem.price) * currentItem.counter)) * 100) / 100;
  }, 0.00);
}

function modifyProductCount(state, action) {
  const { [action.payload.sku]: itemToModify, ...oldCart } = selectInCart(state);
  const { counter } = action.payload;

  itemToModify.counter = ((counter >= 1 && counter <= 999) || isNaN(counter)) ? counter : 1;
  return [itemToModify, oldCart];
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_CART:
      const { [action.payload.sku]: existingItem } = selectInCart(state);
      if (existingItem !== undefined) {
        return state;
      }

      return Object.assign({}, state, {
        inCart: {
          [action.payload.sku]: {
            sku: action.payload.sku,
            price: action.payload.price,
            title: action.payload.title,
            description: action.payload.description,
            counter: 1,
            addedTimestamp: Date.now()
          },
          ...selectInCart(state)
        }
      });
    case REMOVE_FROM_CART:
      const { [action.payload.sku]: removedItem, ...newInCart } = selectInCart(state);
      return Object.assign({}, state, {
        inCart: {
          ...newInCart
        }
      });
    case MODIFY_COUNT:
      const resultState = modifyProductCount(state, action);
      return Object.assign({}, state, {
        inCart: {
          [action.payload.sku]: {
            ...resultState[0]
          },
          ...resultState[1]
        }
      });
    case TOGGLE_CART:
      const cartVisibility = selectCartPreviewVisibility(state);
      return Object.assign({}, state, {
        showCartPreview: !cartVisibility
      });
    default:
      return state;
  }
}

function addToCart(product) {
  const { sku, price, title, description } = product;
  return {
    type: ADD_TO_CART,
    payload: {
      sku,
      price,
      title,
      description
    }
  };
}

function removeFromcart(product) {
  const { sku } = product;
  return {
    type: REMOVE_FROM_CART,
    payload: {
      sku
    }
  };
}

function modifyCount(product, counter) {
  const { sku } = product;
  return {
    type: MODIFY_COUNT,
    payload: {
      sku,
      counter
    }
  };
}

function toggleCart() {
  return {
    type: TOGGLE_CART,
    payload: {}
  };
}

export const cartActions = {
  addToCart,
  modifyCount,
  removeFromcart,
  toggleCart
};
