import React from "react";
import classNames from "classnames";
import Grid from "material-ui/Grid";
import Paper from "material-ui/Paper";
import IconButton from "material-ui/IconButton";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import Divider from "material-ui/Divider";

import CartItemContainer from "./../../shared/cart-item/container/cartItemContainer";

export class CartFull extends React.Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, cartTotal, products } = this.props;
    const cartItemProps = {
      imageUrl: "http://via.placeholder.com/200",
      isPreview: false,
      product: undefined
    };
    const cartHasProducts = products.length > 0;
    const productsDisplay = (cartHasProducts)
      ? products.map((product) => {
        cartItemProps.product = product;
        return (
          <CartItemContainer { ...cartItemProps } />
        );
      })
      : <p className={classes.emptyMessage}>The cart is empty</p>;

    return (
      <Grid container className={classes.cartGridStyle}>
        <Grid item xs={1} />
        <Grid item xs={10} className={classes.cartItemSpacing}>
          <Paper className={classes.cartPaperStyle}>
            <Grid container className={classes.cartHeaderContainer}>
              <Grid item xs={2} lg={1}>
                <Button raised disabled={!cartHasProducts} color="primary">
                  Checkout
                </Button>
              </Grid>
              <Grid item xs={5} lg={8} />
              <Grid item xs={5} lg={3}>
                <Typography type="headline" align="right">
                  Subtotal: ${cartTotal}
                </Typography>
              </Grid>
            </Grid>
            <Divider />
            {
              productsDisplay
            }
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

const cartFullStyles = {
  cartHeaderContainer: {
    padding: "1rem"
  },
  cartItemSpacing: {
    "margin-top": "80px"
  },
  cartPaperStyle: {
    width: "100%",
    "min-height": "180px"
  },
  emptyMessage: {
    "text-align": "center",
    "padding-top": "1rem",
  },
};

export default withStyles(cartFullStyles)(CartFull);
