import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import cartReducer, { cartActions, selectCartItems, selectProductTotal } from "./../cartFull";
import CartFull from "./../component/cartFull";

const mapStateToProps = (state) => {
  const products = selectCartItems(cartReducer(state, {}));
  const cartTotal = selectProductTotal(cartReducer(state, {}));
  return {
    cartTotal,
    products
  };
};

const CartFullContainer = connect(mapStateToProps)(CartFull);

export default CartFullContainer;
