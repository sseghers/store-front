import { combineReducers } from "redux";

import standReducer from "./stand/stand";
import productReducer from "./product/product";
import cartFullReducer from "./cart-full/cartFull";

const rootReducer = combineReducers({
  cart: cartFullReducer,
  stand: standReducer,
  products: productReducer
});

export default rootReducer;
