/* eslint-disable */
import React from "react";
import classNames from "classnames";

import dividerStyle from "./../scss/divider.scss";

export function Divider({ half, margin }) {
  const heightValue = half 
    ? dividerStyle.half_size
    : dividerStyle.full_size;
  const marginValue = margin
    ? dividerStyle.margin
    : dividerStyle.no-margin;

  return <div className={classNames(dividerStyle.vertical_divider, heightValue, marginValue)} />
};
