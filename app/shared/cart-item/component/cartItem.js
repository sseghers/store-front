import React from "react";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import Grid from "material-ui/Grid";
import TextField from "material-ui/TextField";
import IconButton from "material-ui/IconButton";
import DeleteIconButton from "material-ui-icons/Delete";

export class CartItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      productsCounter: {}
    };
    this.handleItemNumberChange = this.handleItemNumberChange.bind(this);
    this.removeFromCart = this.removeFromCart.bind(this);
    this.updateProductCounter = this.updateProductCounter.bind(this);
  }

  counterIsInValid(event) {
    const numericValue = parseInt(event.target.value);
    return numericValue < 1 || numericValue > 999 || isNaN(numericValue);
  }

  updateProductCounter(event, product) {
    const inputIsInvalid = this.counterIsInValid(event);
    if (inputIsInvalid || this.state.productsCounter[product.sku] === undefined) {
      if (!inputIsInvalid) {
        this.updateComponentProductCounter(product,
          product.counter,
          () => this.props.modifyCount(product, this.state.productsCounter[product.sku]));
        return;
      }
      this.updateComponentProductCounter(product, product.counter);
    }

    this.props.modifyCount(product, this.state.productsCounter[product.sku]);
  }

  updateComponentProductCounter(product, value, callback = undefined) {
    this.setState({
      productsCounter: Object.assign({}, this.state.productsCounter, {
        [product.sku]: value
      })
    }, callback);
  }

  handleItemNumberChange(product, event) {
    this.updateComponentProductCounter(product, event.target.value);
  }

  removeFromCart(product) {
    this.props.removeFromcart(product);
  }

  render() {
    const { classes, imageUrl, isPreview, product } = this.props;
    const productCount = (this.state.productsCounter[product.sku] === undefined)
      ? product.counter
      : this.state.productsCounter[product.sku];
    const itemsStyle = (isPreview)
      ? classNames(classes.gridItemStyle, classes.gridItemPreviewHeight)
      : classes.gridItemStyle;
    const gridStyle = (isPreview)
      ? classes.gridStyle
      : classNames(classes.gridStyle, classes.gridStylePreview);
    const deleteButton = (isPreview)
      ? classes.buttons
      : classNames(classes.buttons, classes.deleteButton);
    const flexWidth = {
      counter: (isPreview) ? 2 : 1,
      delete: (isPreview) ? 4 : 3,
      title: (isPreview) ? 4 : 6
    };
    return (
      <Grid container className={gridStyle} key={product.sku}>
        <Grid item xs={2}>
          <img src={imageUrl} className={classes.cardImage} />
        </Grid>
        <Grid item xs={flexWidth.title} className={itemsStyle}>
          <p className={classes.titleStyle}>
            {product.title}
          </p>
        </Grid>
        <Grid item xs={flexWidth.counter}>
          <TextField
              type="number"
              margin="normal"
              onBlur={event => this.updateProductCounter(event, product)}
              onChange={event => this.handleItemNumberChange(product, event)}
              value={productCount}
              className={`${classes.counterStyle}`} />
        </Grid>
        <Grid item xs={flexWidth.delete} className={itemsStyle}>
          <Grid container className={classes.subContainerItemGrid}>
            <Grid item xs={11}>
              <p className={classes.priceStyle}>{product.price}</p>
            </Grid>
          <Grid item xs={7} />
          <Grid item xs={4} className={classes.deleteGridItem}>
            <IconButton className={deleteButton} onClick={() => this.removeFromCart(product)}>
              <DeleteIconButton className={classes.buttons} />
            </IconButton>
            </Grid>
          </Grid>
        </Grid>
    </Grid>
    );
  }
}

const cartItemStyleSheet = {
  buttons: {
    height: "2rem",
    width: "2rem"
  },
  cardImage: {
    "max-width": "100%",
    "max-height": "100%"
  },
  counterStyle: {
    "margin-top": 0
  },
  deleteButton: {
    position: "absolute",
    right: 0,
    bottom: 0
  },
  deleteGridItem: {
    position: "relative"
  },
  gridStyle: {
    padding: "1% 0 1% 1%",
    "overflow-x": "hidden",
    outline: "none",
    width: "100%"
  },
  gridStylePreview: {
    "min-height": "180px"
  },
  gridItemStyle: {
    position: "relative"
  },
  gridItemPreviewHeight: {
    "max-height": "150px"
  },
  priceStyle: {
    "font-weight": "bold",
    "margin-top": 0,
    "margin-bottom": "5%",
    "text-align": "right",
    "font-size": "large"
  },
  titleStyle: {
    margin: 0
  },
  subContainerItemGrid: {
    height: "100%"
  },
};

export default withStyles(cartItemStyleSheet)(CartItem);
