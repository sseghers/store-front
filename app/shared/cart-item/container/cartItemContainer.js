import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import CartItem from "./../component/cartItem";
import { cartActions } from "./../../../cart-full/cartFull";

const { modifyCount, removeFromcart, toggleCart } = cartActions;

const mapStateToProps = (state, ownProps) => {
  return {
    imageUrl: ownProps.imageUrl,
    isPreview: ownProps.isPreview,
    product: ownProps.product
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    modifyCount,
    removeFromcart,
    toggleCart
  }, dispatch);
};

const CartItemContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CartItem);

export default CartItemContainer;
