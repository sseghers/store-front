export const PAGE_REDIRECT = "plinar/navigation/PAGE_REDIRECT";

let initialState;
(function loadInitialState() {
  initialState = {
  };
})();

function createRedirect() {
  return {
    type: PAGE_REDIRECT,
    payload: {}
  };
}

export default function reducer(state = initialState, action) {

}

export const navigationActions = {
  createRedirect
};
