import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Loading from "./../component/loading";
import { requestIsRunning } from "./../loading";

const mapStateToProps = (state) => {
  return {
    showLoader: requestIsRunning(state)
  };
};

const LoadingContainer = connect(
  mapStateToProps
)(Loading);

export default LoadingContainer;
