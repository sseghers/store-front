import { all } from "redux-saga/effects";
import standSagas from "./stand/saga/stand-saga";

export default function* rootSaga() {
  yield all([...standSagas()]);
}
