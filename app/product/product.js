import { getDataStore } from "./../data-source/localStorage";
import { replaceAll } from "./../utility/stringFunctions";
import { toParsedPrice } from "./../utility/formators";

export const UPDATE_DISPLAY_ITEMS = "store_front/product/UPDATE_DISPLAY_ITEMS";
export const GET_DISPLAY_ITEMS = "store_front/product/GET_DISPLAY_ITEMS";
export const RESET_OFFSET = "store_front/product/RESET_OFFSET";
export const SET_PRODUCT_INDEX = "store_front/product/SET_PRODUCT_INDEX";
export const SORT_DISPLAY_ITEMS = "store_front/product/SORT_DISPLAY_ITEMS";
export const LOAD_MORE_DISPLAY_ITEMS = "store_front/product/LOAD_MORE_DISPLAY_ITEMS";

export const SORT_METHODS = {
  PRICE_ASCENDING: "PRICE_ASCENDING",
  PRICE_DESCENDING: "PRICE_DESCENDING",
  TITLE_ASCENDING: "TITLE_ASCENDING",
  TITLE_DESCENDING: "TITLE_DESCENDING"
};

let initialState;
(function loadInitialState() {
  initialState = {
    allCategoryItems: [],
    currentCategory: "",
    displayItems: [],
    index: {},
    offset: 0,
    pageSize: 6,
    allItemsDisplayed: false,
    searchError: {
      isError: false,
      message: ""
    }
  };
})();

export function selectAllCategoryItems(state) {
  return state.allCategoryItems || state.products.allCategoryItems;
}

export function selectCurrentCategory(state) {
  return (state.currentCategory === undefined)
  ? state.products.currentCategory
  : state.currentCategory;
}

export function selectProductsLoaded(state) {
  return (selectAllCategoryItems(state).length > 0)
    || (state.searchError || state.products.searchError).isError;
}

export function selectDisplayItems(state) {
  return state.displayItems || state.products.displayItems;
}

export function selectProductIndex(state) {
  return state.index || state.products.index;
}

export function routeMatchesCategory(params) {
  return !!((params.category && params.category !== "cart") || params.childCategory);
}

export function selectAllItemsDisplayed(state) {
  return state.allItemsDisplayed || state.products.allItemsDisplayed;
}

function handleBadSearch() {
  return {
    searchError: {
      isError: true,
      message: "No products matching search criteria were found."
    }
  };
}

function updateCurrentCategories(state, categories) {
  const { data } = getDataStore();
  let allCategoryItems;
  try {
    let index;
    for (const category of categories) {
      if (index === undefined) {
        index = data[category];
        continue;
      }
      index = index[category];
    }
    if (index === undefined) {
      throw new Error();
    }
    allCategoryItems = index;
  } catch (exception) {
    const searchError = handleBadSearch(exception);
    return {
      searchError
    };
  }

  return {
    allCategoryItems
  };
}

function sortDisplayItemProducts(state, sortMethod) {
  const displayItems = selectDisplayItems(state);
  switch (sortMethod) {
    case SORT_METHODS.PRICE_ASCENDING:
      return displayItems.sort((a, b) => toParsedPrice(a.price) - toParsedPrice(b.price));
    case SORT_METHODS.PRICE_DESCENDING:
      return displayItems.sort((a, b) => toParsedPrice(b.price) - toParsedPrice(a.price));
    case SORT_METHODS.TITLE_ASCENDING:
      return displayItems.sort((a, b) => a.title > b.title);
    case SORT_METHODS.TITLE_DESCENDING:
      return displayItems.sort((a, b) => b.title > a.title);
    default:
      return displayItems;
  }
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_DISPLAY_ITEMS:
      const currentCategory = selectCurrentCategory(state);
      const currentCategoryItems = selectAllCategoryItems(state);

      const categories = action.payload.categories.map(category => replaceAll(category, "_", " "));
      const incomingCategories = categories.join("/");
      if (currentCategory.length === ""
        || currentCategory !== incomingCategories) {
        const { allCategoryItems, searchError } = updateCurrentCategories(state, categories);
        if (searchError !== undefined) {
          return Object.assign({}, state, {
            ...searchError
          });
        }
        const displayItems = allCategoryItems.slice(state.offset, state.pageSize);
        return Object.assign({}, state, {
          allCategoryItems,
          currentCategory: incomingCategories,
          displayItems,
          offset: state.offset + state.pageSize,
          allItemsDisplayed: false
        }, (searchError !== undefined)
          ? {
            ...searchError
          }
          : {});
      }

      const displayItems = currentCategoryItems.slice(state.offset, state.pageSize);
      return Object.assign({}, state, {
        displayItems,
        currentCategory: incomingCategories,
        offset: state.offset + state.pageSize,
        allItemsDisplayed: false
      });
    case LOAD_MORE_DISPLAY_ITEMS:
      const nextCategoryItemSet = selectAllCategoryItems(state).slice(state.offset, state.offset + state.pageSize);
      if (nextCategoryItemSet.length === 0) {
        return Object.assign({}, state, {
          allItemsDisplayed: true
        });
      }
      const currentDisplayItems = selectDisplayItems(state);
      return Object.assign({}, state, {
        displayItems: currentDisplayItems.concat(nextCategoryItemSet),
        offset: state.offset + state.pageSize
      });
    case RESET_OFFSET:
      return Object.assign({}, state, {
        offset: 0
      });
    case SET_PRODUCT_INDEX:
      return Object.assign({}, state, {
        index: action.payload.index
      });
    case SORT_DISPLAY_ITEMS:
      const sortedDisplayItems = sortDisplayItemProducts(state, action.payload.sortMethod);
      return Object.assign({}, state, {
        displayItems: sortedDisplayItems
      });
    default:
      return state;
  }
}

function updateDisplayItems(categories) {
  return {
    type: UPDATE_DISPLAY_ITEMS,
    payload: {
      categories
    }
  };
}

function getDisplayItems() {
  return {
    type: GET_DISPLAY_ITEMS,
    payload: {}
  };
}

function resetOffset() {
  return {
    type: RESET_OFFSET,
    payload: {}
  };
}

function setProductIndex(index) {
  return {
    type: SET_PRODUCT_INDEX,
    payload: {
      index
    }
  };
}

function sortDisplayItems(sortMethod) {
  return {
    type: SORT_DISPLAY_ITEMS,
    payload: {
      sortMethod
    }
  };
}

function loadMoreDisplayItems() {
  return {
    type: LOAD_MORE_DISPLAY_ITEMS,
    payload: {}
  };
}

export const productActions = {
  getDisplayItems,
  loadMoreDisplayItems,
  resetOffset,
  setProductIndex,
  sortDisplayItems,
  updateDisplayItems
};
