import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import productReducer, { productActions, selectAllItemsDisplayed, selectDisplayItems } from "./../product";
import Product from "./../component/product";
import { cartActions, selectProductIsInCart, selectProductCounter } from "./../../cart-full/cartFull";

const { addToCart, removeFromcart } = cartActions;
const { loadMoreDisplayItems } = productActions;

const mapStateToProps = (state) => {
  const products = selectDisplayItems(productReducer(state, {})).map((product) => {
    if (selectProductIsInCart(state, product)) {
      return Object.assign({}, product, {
        isInCart: true,
        counter: selectProductCounter(state, product)
      });
    }
    return Object.assign({}, product, {
      isInCart: false,
      counter: 0
    });
  });
  const allItemsLoaded = selectAllItemsDisplayed(state);

  return {
    allItemsLoaded,
    products
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addToCart,
    loadMoreDisplayItems,
    removeFromcart
  }, dispatch);
};

const ProductContainer = connect(
  mapStateToProps,
  mapDispatchToProps)(Product);

export default ProductContainer;
