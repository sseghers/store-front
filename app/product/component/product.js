import React from "react";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import Card, { CardHeader, CardMedia, CardContent, CardActions } from "material-ui/Card";
import Grid from "material-ui/Grid";
import Button from "material-ui/Button";
import ShoppingCartButton from "material-ui-icons/ShoppingCart";
import InfiniteScroll from "react-infinite-scroller";


import { Divider } from "./../../shared/divider/component/divider";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.handleCartClick = this.handleCartClick.bind(this);
    this.getNextDisplaySet = this.getNextDisplaySet.bind(this);
  }

  handleCartClick(product) {
    if (product.isInCart) {
      this.props.removeFromcart(product);
    } else {
      this.props.addToCart(product);
    }
  }

  getDisplayProductsRender(products) {
    const { classes } = this.props;
    return products.map((product) => {
      const imageUrl = "https://via.placeholder.com/200";
      const cartButtonStyle = (product.isInCart)
        ? { type: "remove", color: "accent" }
        : { type: "add", color: "primary" };
      return <Card className={classes.cardStyle} key={product.sku}>
          <Grid container className={classes.cardContainer}>
            <Grid item xs={3} className={classes.cardImageFrame}>
              <img src={imageUrl} className={classes.cardImage}/>
            </Grid>
            <Grid item xs={5}>
              <CardHeader title={product.title} />
              <p className={classes.description}>{product.description}</p>
            </Grid>
            <Grid item xs={3} className={classes.priceFrame}>
              <p className={classes.price}>{product.price}</p>
            </Grid>
            <Grid item xs={10} />
            <Grid item xs={2}>
              <Button raised color={cartButtonStyle.color}
                className={classNames(classes.cartButton)}
                onClick={() => this.handleCartClick(product)}>
                  <i className="material-icons">{cartButtonStyle.type}</i>
              </Button>
            </Grid>
          </Grid>
        </Card>;
    });
  }

  getNextDisplaySet() {
    this.props.loadMoreDisplayItems();
  }

  render() {
    const { allItemsLoaded, classes, products } = this.props;
    const renderProducts = this.getDisplayProductsRender(products);
    const hasMore = !allItemsLoaded;
    return (
      <Grid container className={classes.gridContainer}>
        <Grid item xs={12}>
          <InfiniteScroll
          hasMore={hasMore}
          loadMore={this.getNextDisplaySet}
          useWindow={true}>
            {renderProducts}
          </InfiniteScroll>
        </Grid>
      </Grid>
    );
  }
}

const scssConstants = {
  cardElementSizing: {
    margin: "2rem"
  }
};

const standStyleSheet = {
  buttons: {
    height: "3rem",
    width: "3rem"
  },
  cardStyle: {
    "margin-bottom": scssConstants.cardElementSizing.margin,
    "&:last-child": {
      "margin-bottom": "15%"
    }
  },
  cardContainer: {
    padding: "1% 0 1% 1%",
    "max-width": "100%"
  },
  cardImageFrame: {
  },
  cardImage: {
    "max-width": "100%"
  },
  cartButton: {
    float: "right",
  },
  description: {
    color: "#5A5A5A"
  },
  gridContainer: {
    width: "100%"
  },
  priceFrame: {
    position: "relative",
    "margin-left": "5vw"
  },
  price: {
    float: "right",
    "font-weight": "bold",
    "font-size": "20px"
  },
  productInCart: {
    "background-color": "#7CB342"
  }
};

export default withStyles(standStyleSheet)(Product);
