import React from "react";
import { withRouter } from "react-router";
import classNames from "classnames";
import Menu, { MenuItem } from "material-ui/Menu";
import Grid from "material-ui/Grid";
import { withStyles } from "material-ui/styles";
import IconButton from "material-ui/IconButton";
import Input from "material-ui/Input/Input";
import Paper from "material-ui/Paper";
import Divider from "material-ui/Divider";
import ArrowDownCircle from "material-ui-icons/ArrowDropDownCircle";
import Chip from "material-ui/Chip";
import List, { ListItem } from "material-ui/List";

import CartItemContainer from "./../../shared/cart-item/container/cartItemContainer";

export class CartPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCartPreview: false,
      cartElement: undefined
    };
    this.redirectCartFull = this.redirectCartFull.bind(this);
  }

  redirectCartFull() {
    this.props.history.push("/cart");
    this.props.toggleCart();
  }

  render() {
    const { cartElement, cartTotal, classes, products, showCartPreview, toggleCart } = this.props;
    const cartItemProps = {
      imageUrl: "http://via.placeholder.com/200",
      isPreview: true,
      product: undefined
    };
    const productsInCart = products.length > 0;
    const cartDisplay = (productsInCart)
      ? products.slice(0, 3).map((product) => {
        cartItemProps.product = product;
        return (
          <CartItemContainer { ...cartItemProps }/>
        );
      })
    : (
      <Grid container className={classes.gridStyle}>
        <Grid item xs={11} className={classNames(classes.gridItemStyle, classes.emptyCart)} />
      </Grid>
      );
    return (
      <div>
        <Menu
          open={showCartPreview}
          anchorEl={cartElement}
          onRequestClose={toggleCart}
          className={classes.cartPreview}>
          {cartDisplay}
          <Divider />
          <List className={classes.footerListStyle}>
            <ListItem className={classes.previewFooter}>
              <Grid container>
                <Grid item xs={5} />
                <Grid item xs={2}>
                  <IconButton className={classes.buttons} onClick={this.redirectCartFull}>
                    <ArrowDownCircle className={classes.buttons}/>
                  </IconButton>
                </Grid>
                <Grid item xs={3} />
                <Grid item xs={2}>
                  <Chip label={`$${cartTotal}`} className={classes.cartTotal} />
                </Grid>
              </Grid>
            </ListItem>
          </List>
        </Menu>
      </div>
    );
  }
}

const cartPreviewStyleSheet = {
  buttons: {
    height: "2rem",
    width: "2rem"
  },
  cartPreview: {
    position: "fixed",
    top: "75px !important",
    width: "45vw",
    "font-family": '"Roboto", "Helvetica", "Arial", sans-serif;',
    "z-index": "5"
  },
  cartTotal: {
    float: "right"
  },
  emptyCart: {
    "min-height": "80px"
    // "text-align": "center",
    // "line-height": "8vh"
  },
  footerListStyle: {
    outline: "none",
    "overflow-y": "hidden",
    height: "18px"
  },
  gridStyle: {
    padding: "1% 0 1% 1%",
    "overflow-x": "hidden",
    outline: "none",
    width: "100%"
  },
  gridItemStyle: {
    "max-height": "150px",
    position: "relative"
  },
  previewFooter: {
    height: "0"
  },
  redirectCartButtonStyle: {
    height: "2rem"
  }
};

export default withRouter(withStyles(cartPreviewStyleSheet)(CartPreview));
