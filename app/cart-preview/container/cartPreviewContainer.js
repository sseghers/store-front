import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Cart from "./../component/cartPreview";
import cartReducer, { cartActions, selectCartPreviewVisibility, selectCartItems, selectProductTotal } from "./../../cart-full/cartFull";
import productReducer from "./../../product/product";

const { modifyCount, removeFromcart, toggleCart } = cartActions;

const mapStateToProps = (state) => {
  const showCartPreview = selectCartPreviewVisibility(cartReducer(state, {}));
  const products = selectCartItems(cartReducer(state, {}));
  const cartTotal = selectProductTotal(cartReducer(state, {}));
  return {
    cartTotal,
    products,
    showCartPreview
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    modifyCount,
    removeFromcart,
    toggleCart
  }, dispatch);
};

const CartPreviewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);

export default CartPreviewContainer;
