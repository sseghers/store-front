import { standActions } from "./../stand/stand";

const { dataStoreLoaded, loadInitialDataStore } = standActions;

export const DATA_STATE = "dataState";
export const STORE_STATE = "storeState";

// store.subscribe(throttle(() => {
//   saveState(store.getState());
// }, 1000));

export const loadState = (storeType, reduxStore = undefined) => {
  const reduxAware = reduxStore !== undefined;
  try {
    const state = localStorage.getItem(storeType);
    if (state === null) {
      if (reduxAware) {
        reduxStore.dispatch(loadInitialDataStore());
      }
      return undefined;
    }

    if (reduxAware) {
      reduxStore.dispatch(dataStoreLoaded());
    }

    return JSON.parse(state);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (storeType, state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(storeType, serializedState);
  } catch (error) {
    console.log(error);
    return false;
  }
  return true;
};

export const getDataStore = () => {
  try {
    const dataState = localStorage.getItem(DATA_STATE);
    if (dataState === null) {
      return undefined;
    }
    return JSON.parse(dataState);
  } catch (error) {
    console.log(error);
    return undefined;
  }
};
