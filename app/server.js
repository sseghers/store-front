const express = require("express");
const helmet = require("helmet");
const path = require("path");

const app = express();
const port = 8080;

app.use(helmet());

const indexPath = path.join(__dirname, "/../public/index.html");
const publicPath = express.static(path.join(__dirname, "../public"));
app.use("/public", publicPath);

const serverIndex = (req, res) => res.sendFile(indexPath);
app.get("/*", serverIndex);

app.listen((process.env.PORT || port), (error) => {
  if (error) {
    console.log(error);
  } else {
    console.log(__dirname);
    console.info("Listening on "+ port); // eslint-disable-line 
  }
});
