/* eslint import/first: 0 */
import React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import throttle from "lodash/throttle";

import App from "./app";
import { loadState, saveState } from "./data-source/localStorage";
import configureStore from "./configureStore";
import rootSaga from "./sagas-index";
import { DATA_STATE, STORE_STATE } from "./data-source/localStorage";

const state = loadState(STORE_STATE);
const store = configureStore(state);
store.runSaga(rootSaga);

loadState(DATA_STATE, store);

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
