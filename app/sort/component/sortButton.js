import React from "react";
import classNames from "classnames";
import Button from "material-ui/Button";
import Menu, { MenuItem } from "material-ui/Menu";
import { withStyles } from "material-ui/styles";

class SortButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      anchorEl: undefined,
      sortOpen: false
    };
    this.toggleSortOptions = this.toggleSortOptions.bind(this);
    this.sortItems = this.sortItems.bind(this);
  }

  setSortAnchorElement(event) {
    this.setState({
      anchorEl: event.currentTarget
    });
  }

  toggleSortOptions(openState, event = undefined) {
    if (event) {
      this.setSortAnchorElement(event);
    }
    this.setState({
      sortOpen: openState
    });
  }

  sortItems(sortMethod) {
    this.props.sortDisplayItems(sortMethod);
    this.toggleSortOptions(false);
  }

  render() {
    const { anchorEl, sortOpen } = this.state;
    const { classes, matchesQuery, sortMethods } = this.props;
    const buttonStyle = matchesQuery
    ? classes.mobileSortButton
    : classes.desktopSortButton;

    const { PRICE_ASCENDING, PRICE_DESCENDING,
      TITLE_ASCENDING, TITLE_DESCENDING } = sortMethods;

    return (
      <div>
        <Menu anchorEl={anchorEl}
          open={sortOpen}
          onRequestClose={() => this.toggleSortOptions(false)}>
          <MenuItem onClick={() => this.sortItems(PRICE_ASCENDING)}>
            $ - $$$
          </MenuItem>
          <MenuItem onClick={() => this.sortItems(PRICE_DESCENDING)}>
            $$$ - $
          </MenuItem>
          <MenuItem onClick={() => this.sortItems(TITLE_ASCENDING)}>
            A - Z
          </MenuItem>
          <MenuItem onClick={() => this.sortItems(TITLE_DESCENDING)}>
            Z - A
          </MenuItem>
        </Menu>
        <Button fab className={buttonStyle} onClick={event => this.toggleSortOptions(true, event)}>
          <i className="material-icons">sort</i>
        </Button>
      </div>
    );
  }
}

const scssConstants = {
  accessoryColor: "#ffb74d"
};

const sortButtonStyleSheet = {
  desktopSortButton: {
    position: "fixed",
    top: "15%",
    left: "28px",
    "background-color": scssConstants.accessoryColor
  },
  mobileSortButton: {
    "background-color": scssConstants.accessoryColor,
    bottom: "28px"
  }
};

export default withStyles(sortButtonStyleSheet)(SortButton);
