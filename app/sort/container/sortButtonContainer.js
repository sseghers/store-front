import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import SortButton from "./../component/sortButton";
import { productActions, SORT_METHODS } from "./../../product/product";

const { sortDisplayItems } = productActions;

const mapStateToProps = (state, ownProps) => {
  const { mobile } = ownProps;
  return {
    matchesQuery: mobile,
    sortMethods: SORT_METHODS
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    sortDisplayItems
  }, dispatch);
};

const ProductContainer = connect(
  mapStateToProps,
  mapDispatchToProps)(SortButton);

export default ProductContainer;
