import React from "react";
import { connect } from "react-redux";
import { render } from "react-dom";
import { Switch, Route } from "react-router-dom";
import { MuiThemeProvider, MuiTheme } from "material-ui/styles";

import StandContainer from "./stand/container/standContainer";
import CartFullContainer from "./cart-full/container/cartFullContainer";
import Globals from "./../scss/globals.scss";

export default class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Route path="/:category?/:childCategory?" component={StandContainer} />
          <Route path="/cart" component={CartFullContainer} />
        </div>
      </MuiThemeProvider>
    );
  }
}
