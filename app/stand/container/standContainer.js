import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import productReducer, { productActions, routeMatchesCategory, selectProductsLoaded, selectProductIndex } from "./../../product/product";
import cartReducer, { cartActions, selectCartPreviewVisibility, selectCartProductCount } from "./../../cart-full/cartFull";
import standReducer, { selectDataStoreLoaded } from "./../stand";
import { getDataStore } from "./../../data-source/localStorage";
import Stand from "./../component/stand";

const { resetOffset, setProductIndex, updateDisplayItems } = productActions;
const { toggleCart } = cartActions;

const mapStateToProps = (state) => {
  let index = selectProductIndex(state, {});
  if (Object.keys(index).length < 1) {
    const dataStore = getDataStore();
    index = (dataStore !== undefined) ? dataStore.index : {};
  }

  const productsAreLoaded = selectProductsLoaded(productReducer(state, {}));
  const showCartPreview = selectCartPreviewVisibility(cartReducer(state, {}));
  const cartCounter = selectCartProductCount(cartReducer(state, {}));
  const dataLoaded = selectDataStoreLoaded(standReducer(state, {}));
  return {
    cartCounter,
    dataLoaded,
    index,
    productsAreLoaded,
    showCartPreview,
    routeMatchesCategory
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    resetOffset,
    setProductIndex,
    toggleCart,
    updateDisplayItems,
  }, dispatch);
};

const StandContainer = connect(mapStateToProps, mapDispatchToProps)(Stand);

export default StandContainer;
