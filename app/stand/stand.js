export const LOAD_INITIAL_DATA_STORE = "store_front/stand/LOAD_INITIAL_DATA_STORE";
export const DATA_STORE_LOADED = "store_front/stand/DATA_STORE_LOADED";

let initialState;
(function loadInitialState() {
  initialState = {
    dataLoaded: false
  };
})();

export function selectDataStoreLoaded(state) {
  return state.dataLoaded || state.stand.dataLoaded;
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case DATA_STORE_LOADED:
      return Object.assign({}, state, {
        dataLoaded: true
      });
    default:
      return state;
  }
}

function loadInitialDataStore() {
  return {
    type: LOAD_INITIAL_DATA_STORE,
    payload: {}
  };
}

function dataStoreLoaded() {
  return {
    type: DATA_STORE_LOADED,
    payload: {}
  };
}

export const standActions = {
  dataStoreLoaded,
  loadInitialDataStore
};
