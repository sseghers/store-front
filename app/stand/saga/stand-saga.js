import "whatwg-fetch";
import { take, put, call, fork, select } from "redux-saga/effects";

import { standActions, LOAD_INITIAL_DATA_STORE } from "./../stand";
import { saveState, DATA_STATE } from "./../../data-source/localStorage";

const { dataStoreLoaded, loadInitialStore } = standActions;

const isProduction = process.env.NODE_ENV === "production";

function* loadInitialDataStore() {
  const resourceUrl = (isProduction) ? "/public/sample_data.json" : "/sample_data.json";
  const request = () => fetch(resourceUrl,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then((resp) => {
      return resp.json();
    })
    .then((data) => {
      return data;
    });

  const response = yield call(request);
  if (response) {
    if (saveState(DATA_STATE, response)) {
      yield put(dataStoreLoaded());
    }
  }
}

function* watchLoadInitialDataStore() {
  while (true) {
    yield take(LOAD_INITIAL_DATA_STORE);
    yield fork(loadInitialDataStore);
  }
}

export default function standSagas() {
  return [
    fork(watchLoadInitialDataStore)
  ];
}
