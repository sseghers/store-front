import React from "react";
import { withRouter } from "react-router";
import classNames from "classnames";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Paper from "material-ui/Paper";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import IconButton from "material-ui/IconButton";
import ViewListButton from "material-ui-icons/ViewList";
import Drawer from "material-ui/Drawer";
import List, { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import ShoppingCartButton from "material-ui-icons/ShoppingCart";
import ChevronLeftButton from "material-ui-icons/ChevronLeft";
import HomeButton from "material-ui-icons/Home";
import Grid from "material-ui/Grid";
import Button from "material-ui/Button";
import Badge from "material-ui/Badge";
import { Route, Switch } from "react-router-dom";
import Media from "react-media";

import ProductContainer from "./../../product/container/productContainer";
import { toUpperCaseFirst } from "./../../utility/formators";
import { replaceAll } from "./../../utility/stringFunctions";
import CartPreviewContainer from "./../../cart-preview/container/cartPreviewContainer";
import SortButtonContainer from "./../../sort/container/sortButtonContainer";

class Stand extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      baseCategories: [],
      open: {
        [this.baseDrawer]: false,
        [this.secondDrawer]: false
      },
      childCategories: [],
      selectedCategory: [],
      cartElement: undefined
    };
    this.openCategoryBase = this.openCategoryBase.bind(this);
    this.closeCategoryBase = this.closeCategoryBase.bind(this);
    this.openChildCategory = this.openChildCategory.bind(this);
    this.closeChildCategory = this.closeChildCategory.bind(this);
    this.closeAllDrawers = this.closeAllDrawers.bind(this);
    this.redirectWithCategories = this.redirectWithCategories.bind(this);
    this.toggleCartPreview = this.toggleCartPreview.bind(this);
    this.redirectHome = this.redirectHome.bind(this);
    const { dataLoaded } = props;
    if (dataLoaded) {
      this.updateComponentData();
    }
  }

  componentDidMount() {
    const standBase = {
      height: "100%",
      backgroundColor: "#1565c0"
    };
    document.body.style.height = standBase.height;
    document.body.style.backgroundColor = standBase.backgroundColor;
    document.body.style.margin = "0";
  }

  updateComponentData() {
    const { index } = this.props;
    this.props.setProductIndex(index);
    this.state.baseCategories = Object.keys(index).map(toUpperCaseFirst);
  }

  get baseDrawer() {
    return "baseDrawer";
  }

  get secondDrawer() {
    return "childCategories";
  }

  toggleCartPreview(event) {
    if (this.state.cartElement === undefined) {
      this.state.cartElement = event.currentTarget;
    }
    this.props.toggleCart();
  }

  toggleDrawer(drawerLevel, status) {
    const drawerState = Object.assign({}, this.state.open);
    drawerState[drawerLevel] = status;
    this.setState({ open: drawerState });
  }

  openCategoryBase() {
    this.toggleDrawer(this.baseDrawer, true);
  }

  closeCategoryBase() {
    this.toggleDrawer(this.baseDrawer, false);
  }

  openChildCategory(event) {
    this.state.selectedCategory[0] = event.target.innerText.toLowerCase().trim();
    this.state.childCategories = this.props.index[this.state.selectedCategory[0]].map(toUpperCaseFirst);
    if (this.state.childCategories.length > 0) {
      this.toggleDrawer(this.secondDrawer, true);
    } else {
      this.redirectWithCategories();
    }
  }

  closeChildCategory() {
    this.toggleDrawer(this.secondDrawer, false);
  }

  closeAllDrawers() {
    this.state.selectedCategory = [];
    this.closeChildCategory();
    const context = this;
    setTimeout(context.closeCategoryBase, 0);
  }

  updateSelectedCategories(childCategory = "", baseCategory = "") {
    if (baseCategory !== "") {
      this.state.selectedCategory[0] = baseCategory;
    }
    if (childCategory !== "") {
      this.state.selectedCategory[1] = childCategory;
    }
  }

  updateDisplayItems() {
    this.props.resetOffset();
    this.props.updateDisplayItems(this.state.selectedCategory);
  }

  redirectWithCategories(event = "") {
    if (event !== "") {
      this.updateSelectedCategories(event.target.innerText.toLowerCase().trim());
    } else {
      this.updateSelectedCategories();
    }

    this.updateDisplayItems();
    this.props.history.push(`/${replaceAll(this.state.selectedCategory.join("/"), " ", "_")}`);
    this.closeAllDrawers();
  }

  redirectHome() {
    this.props.history.push("/");
  }

  getResponsiveBar() {
    const { classes } = this.props;
    return (
      <Media query="screen and (min-device-width: 600px)">
        {
          (matches) => {
            return matches ? (
            <Grid item sm={1} className={classes.desktopAppBar}>
              <SortButtonContainer />
            </Grid>
          )
          : (
            <Grid item>
              <AppBar position="fixed" className={classes.mobileAppBar}>
                <SortButtonContainer mobile/>
              </AppBar>
            </Grid>
          );
          }
        }
      </Media>
    );
  }

  render() {
    const { cartCounter, classes, dataLoaded, match, productsAreLoaded, showCartPreview } = this.props;

    if (!dataLoaded) {
      return (<div></div>);
    }

    this.updateComponentData();
    const isCategorySpecificRoute = this.props.routeMatchesCategory(match.params);
    const baseCategories = this.state.baseCategories;
    const childCategories = this.state.childCategories;
    const { category, childCategory } = match.params;

    const updateProductData = (category || (category && childCategory)) && !productsAreLoaded;
    if (updateProductData) {
      this.updateSelectedCategories(childCategory, category);
      this.updateDisplayItems();
    }

    const showProducts = isCategorySpecificRoute && productsAreLoaded;
    const responsiveAppBar = this.getResponsiveBar();

    return (
      <div>
        <Grid container>
          <Grid item xs={12}>
            <AppBar position="fixed" className={classes.appBar}>
              <Toolbar>
                <Grid item xs={1}>
                  <IconButton className={classes.buttonDimensions} onClick={this.openCategoryBase}>
                    <ViewListButton className={classes.button}/>
                  </IconButton>
                </Grid>
                <IconButton className={classNames(classes.buttonDimensions, classes.homeButton)} onClick={this.redirectHome}>
                  <HomeButton className={classes.button}/>
                </IconButton>
                <Grid item xs={10} />
                <Grid item xs={1}>
                  <Badge badgeContent={cartCounter} classes={{ badge: classes.badge }}>
                    <IconButton className={classNames(classes.buttonDimensions, classes.cartPositioning)} >
                      <ShoppingCartButton className={classes.button} onClick={this.toggleCartPreview} />
                    </IconButton>
                  </Badge>
                  {
                    showCartPreview && <CartPreviewContainer cartElement={this.state.cartElement} />
                  }
                </Grid>
              </Toolbar>
            </AppBar>
          </Grid>
        </Grid>
        <Drawer open={this.state.open[this.baseDrawer]} onRequestClose={this.closeAllDrawers} classes={{ paper: classes.drawerPaper }}>
        {
          baseCategories.map((mapCategory) => {
            return (
              <ListItem button key={mapCategory} onClick={event => this.openChildCategory(event)}>
                <ListItemText primary={mapCategory} />
              </ListItem>
            );
          })
        }
        </Drawer>
        <Drawer open={this.state.open[this.secondDrawer]} onRequestClose={this.closeAllDrawers} classes={{ paper: classes.drawerPaper }}>
        <IconButton>
            <ChevronLeftButton onClick={this.closeChildCategory}/>
        </IconButton>
        {
          (childCategories && childCategories.length > 0) &&
          childCategories.map((mapCategory) => {
            return (
              <ListItem button key={mapCategory} onClick={event => this.redirectWithCategories(event)}>
                  <ListItemText primary={mapCategory} />
              </ListItem>
            );
          })
        }
        </Drawer>
        {
          showProducts &&
          <Grid container>
            {
              responsiveAppBar
            }
          <Grid item xs={10} className={classes.productsLane}>
            <div className={classes.gridPositionElement} />
            <ProductContainer />
          </Grid>
        </Grid>
        }
      </div>
    );
  }
}

const scssConstants = {
  buttons: {
    height: "2rem",
    width: "2rem",
    color: "#FFFFFF"
  },
  accessoryColor: "#ffb74d"
};

const standStyleSheet = {
  appBar: {
    backgroundColor: "#003c8f",
    position: "fixed",
    "box-sizing": "border-box"
  },
  badge: {
    "background-color": scssConstants.accessoryColor
  },
  buttonDimensions: {
    height: scssConstants.buttons.height,
    width: scssConstants.buttons.width,
    color: scssConstants.buttons.color
  },
  button: {
    height: scssConstants.buttons.height,
    width: scssConstants.buttons.width
  },
  desktopAppBar: {
    "background-color": "#003c8f",
    "max-height": "100%",
    "max-width": "64px",
    "min-width": "64px"
  },
  drawerPaper: {
    width: "200px",
  },
  gridPositionElement: {
    "margin-top": "80px"
  },
  homeButton: {
    position: "absolute",
    "margin-left": "45%"
  },
  mobileAppBar: {
    bottom: "0",
    backgroundColor: "#003c8f",
    "box-sizing": "border-box",
    top: "auto"
  },
  productsLane: {
    "background-color": "#1565c0",
    "margin-left": "5%"
  }
};

export default withRouter(withStyles(standStyleSheet)(Stand));
