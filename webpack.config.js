const webpack = require("webpack");
const path = require("path");
const DashboardPlugin = require("webpack-dashboard/plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const paths = require("./config/webpack").paths;
const outputFiles = require("./config/webpack").outputFiles;
const rules = require("./config/webpack").rules;
const plugins = require("./config/webpack").plugins;
const resolve = require("./config/webpack").resolve;
const IS_PRODUCTION = require("./config/webpack").IS_PRODUCTION;
const IS_DEVELOPMENT = require("./config/webpack").IS_DEVELOPMENT;
const devServer = require("./config/webpack").devServer;
const pathHelper = require("./config/path-helper");

const entry = [
  "babel-polyfill",
  path.join(paths.javascript, "index.js")
];

plugins.push(
  // Enables HMR
  new webpack.HotModuleReplacementPlugin(),
  // Don"t emmit build when there was an error while compiling
  // No assets are emitted that include errors
  new webpack.NoEmitOnErrorsPlugin(),
  // Webpack dashboard plugin
  new DashboardPlugin()
);

// In development we add "react-hot-loader" for .js/.jsx files
// Check rules in config.js
rules[0].loaders.unshift("react-hot-loader/webpack");
entry.unshift("react-hot-loader/patch");

// Webpack config
module.exports = {
  devtool: IS_PRODUCTION ? false : "cheap-eval-source-map",
  context: paths.javascript,
  watch: !IS_PRODUCTION,
  entry,
  output: {
    path: paths.build,
    publicPath: "/",
    filename: outputFiles.client,
  },
  module: {
    rules,
  },
  resolve,
  plugins,
  devServer
};
