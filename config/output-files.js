const outputFiles = {
  client: "app.js",
  vendor: "vendor.js",
  css: "style.css",
};

module.exports = {
  outputFiles
};