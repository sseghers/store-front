const webpack = require("webpack");
const path = require("path");
const pathHelper = require("./path-helper");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const autoprefixer = require("autoprefixer");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const outputFiles = require("./output-files").outputFiles;

const paths = {
  javascript: path.join(__dirname, "../app"),
  build: path.join(__dirname, "../dist")
  // images: path.join(__dirname, "../source/assets/img"),
  // svg: path.join(__dirname, "../source/assets/svg"),
};

const NODE_ENV = process.env.NODE_ENV || "development";
const SERVER_RENDER = process.env.SERVER_RENDER === "false";
const IS_DEVELOPMENT = NODE_ENV === "development";
const IS_PRODUCTION = NODE_ENV === "production";

// ----------
// PLUGINS
// ----------
// Shared plugins
const plugins = [
  // Extracts CSS to a file
  new ExtractTextPlugin(outputFiles.css),
  // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
  // inside your code for any environment checks; UglifyJS will automatically
  // drop any unreachable code.
  new webpack.DefinePlugin({
    "process.env": {
      NODE_ENV: JSON.stringify(NODE_ENV),
      SERVER_RENDER: JSON.stringify(SERVER_RENDER) === "true",
    },
  }),
  new CopyWebpackPlugin([
    {
      from:  path.resolve(pathHelper.root("favicon.ico")),
      to: "favicon.ico"
    },
    {
      from: path.resolve(pathHelper.root("app/data-source/sample_data.json")),
      to: "sample_data.json"
    }
  ])
];

plugins.push(
  // Creates vendor chunk from modules coming from node_modules folder
  new webpack.optimize.CommonsChunkPlugin({
    name: "vendor",
    filename: outputFiles.vendor,
    minChunks(module) {
      const context = module.context;
      return context && context.indexOf("node_modules") >= 0;
    },
  }),
  // Builds index.html from template
  new HtmlWebpackPlugin({
    template: "../index.html"
  })
);

if (IS_PRODUCTION) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      uglifyOptions: {
        ie8: false,
        ecma: 8,
        compress: {
          comparisons: true,
          conditionals: true,
          dead_code: true,
          drop_console: !SERVER_RENDER, // Keep server logs
          drop_debugger: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
          screw_ie8: true,
          sequences: true,
          unused: true,
          warnings: false
        },
        output: {
          comments: false
        },
        warnings: true
      }
    })
);
} else {
  // Shared development plugins
  plugins.push(
    // Enables pretty names instead of index
    new webpack.NamedModulesPlugin()
  );
}

// ----------
// RULES
// ----------
// Shared rules
const rules = [
  // Babel loader without react hot loader
  // react-hot-loader will is added in webpack.config.js for development only
  {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loaders: [
      {
        loader: "babel-loader",
        query: {
          presets: ["react"]
        }
      }]
  },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: "eslint-loader",
    options: {
      quiet: true // Silence warnings in both console and browser
    }
  },
  {
    test: /\.css$/,
    exclude: /node_modules/,
    loader: ExtractTextPlugin.extract({
      fallback: "style-loader",
      use: "css-loader?modules&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader"
    }),
  },
  {
    test: /\.s(c|a)ss$/,
    exclude: [
      /node_modules/,
      path.resolve(pathHelper.root("scss/globals.scss"))
    ],
    loader: ExtractTextPlugin.extract({
      fallback: "style-loader",
      use: "css-loader?modules&importLoader=2&sourceMap&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader!sass-loader"
    }),
  },
  {
    test: /\.s(c|a)ss$/,
    include: [
      /node_modules/,
      path.resolve(pathHelper.root("scss/globals.scss"))
    ],
    loader: ExtractTextPlugin.extract({
      fallback: "style-loader",
      use: "css-loader!sass-loader"
    }),
  },
  {
    test: /\.(eot|svg|ttf|woff|woff2|ico)$/,
    loader: "file-loader?name=[name].[ext]",
  },
  {
    test: /\.(jpg|png|gif)$/,
    loaders: [
      "file-loader",
      {
        loader: "image-webpack-loader",
        query: {
          progressive: true,
          optimizationLevel: 7,
          interlaced: false,
          pngquant: {
            quality: "65-90",
            speed: 4,
          },
        },
      },
    ],
  },
  {
    test: /\.html$/,
    loader: "html-loader",
  },
  {
    test: /\.json$/,
    loader: "json-loader",
  },
  {
    test: /\.(mp4|webm)$/,
    loader: "url-loader",
    query: {
      limit: 10000,
    },
  }
];

const resolve = {
  extensions: [".js", ".jsx", ".webpack-loader.js", ".web-loader.js", ".loader.js", ".react.js", ".json"],
  modules: [ paths.javascript, "../node_modules"]
};

const devServer = {
  contentBase: IS_PRODUCTION ? paths.build : paths.javascript,
  historyApiFallback: true,
  port: 8080,
  compress: IS_PRODUCTION,
  // inline: !IS_PRODUCTION,
  hot: !IS_PRODUCTION,
  host: "localhost",
  disableHostCheck: true, // To enable local network testing
  // overlay: true,
  stats: {
    assets: true,
    children: false,
    chunks: false,
    hash: false,
    modules: false,
    publicPath: false,
    timings: true,
    version: false,
    warnings: true,
    colors: true,
  },
};

module.exports = {
  outputFiles,
  paths,
  plugins,
  resolve,
  rules,
  IS_DEVELOPMENT,
  IS_PRODUCTION,
  NODE_ENV,
  SERVER_RENDER,
  devServer
};
